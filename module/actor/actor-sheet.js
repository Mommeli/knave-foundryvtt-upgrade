/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class knaveActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    let w = 800
    let h = 550

    return mergeObject(super.defaultOptions, {
      classes: ["mouses-knave", "sheet", "actor"],
      template: "systems/mouses-knave/templates/actor/actor-sheet.html",
      width: w,
      height: h,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
    });
  }

  /* -------------------------------------------- */

  /** 

  /**
   * Change the actor sheet finding system to support multiple types
   * 
   * @override
   */
  get template() {
    const path = "systems/mouses-knave/templates/actor";
    // Return a single sheet for all item types.
    // return `${path}/item-sheet.html`;

    // Alternatively, you could use the following return statement to do a
    // unique item sheet by type, like `weapon-sheet.html`.

    return `${path}/${this.actor.data.type}-sheet.html`;
  }

  /** @override */
  getData() {
    const context = super.getData();
    /* data.dtypes = ["String", "Number", "Boolean"];
    for (let attr of Object.values(data.data.attributes)) {
      attr.isCheckbox = attr.dtype === "Boolean";
    } */

    // Prepare items.
    if (context.actor.data.type == 'character') {
      this._prepareCharacterItems(context);
      this._prepareCharacterData(context);
    }

    //console.log(data)

    return context;
  }

  _prepareCharacterData(sheetData) {
    const data = sheetData.data.data;

    // Make modifications to data here. For example:

    // Loop through ability scores, and add their modifiers to our sheet output.
    for (let [key, ability] of Object.entries(data.abilities)) {
      // Calculate the modifier using d20 rules.
      //ability.mod = Math.floor((ability.value - 10) / 2);
      data.abilities[key].bonus = ability.defense - 10;
    }

    data.attributes.armor.bonus = data.attributes.armor.defense - 10;

    //this.actor = data

    //console.log(this.actor)
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.data.data;
    //console.log(actorData)


    //console.log(actorData)

    // Initialize containers.

    // Iterate through items, allocating to containers
    // let totalWeight = 0;
    let totalSlots = 0;
    for (let i of sheetData.items) {
      let item = i.data;
      totalSlots += Number(item.slotstaken);
    }

    // Add weight from coins
    if (game.settings.get("mouses-knave", "coinsHaveWeight")) {
      let totalCoins = actorData.coins.copper + actorData.coins.silver + actorData.coins.gold + actorData.coins.platinum
      let coinSlots = (totalCoins - (totalCoins % 100)) / 100

      totalSlots += coinSlots
    }
    
    //console.log(totalSlots);

    actorData.attributes.slotsremaining = actorData.abilities.con.defense - totalSlots;
  
  }

  /* -------------------------------------------- */

  /** @override */
  async activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      let context = this.getData()
      //console.log(context.actor.items)

      const li = $(ev.currentTarget)

      //const li = $(ev.currentTarget).parents(".item");
      let item = context.actor.items.get(li.data("item-id"));
      //console.log(item);
      item.sheet.render(true)
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      ev.stopPropagation(); // This makes it so we can delete and item without it popping up

      let context = this.getData()
      //console.log(context.actor.items)

      
      const li = $(ev.currentTarget).parents(".item");
      let item = context.actor.items.get(li.data("item-id"));
      item.delete()
      li.slideUp(200, () => this.render(false));
    });

    // Rollable abilities.
    html.find('.rollable').click(this._onRoll.bind(this));


    // Steal Item Dragging Code
    let handler = ev => this._onDragItemStart(ev);
    html.find('.item-row').each((index, itemRow) => {
      if (itemRow.classList.contains("item-head-row")) return;
      itemRow.setAttribute("draggable", true);
      itemRow.addEventListener("dragstart", handler, false);
    });


    /* AUTOMATIC GENERATION */
    html.find('.generate-character').click( async (ev) => {
      //console.log(ev);
      console.log("Let's generate this baby!!");
    
      let context = this.getData();
      let act = context.actor;

      /* Generate Ability Scores */
      console.log("Character Generating: Ability Scores")
      
      let abilis = duplicate(act.data.data.abilities);
      
      for (let [key, ability] of Object.entries(abilis)) {
        let abilityRoll = new Roll("3d6dh2")
        await abilityRoll.roll()
        
        //console.log(abilityRoll.result);
        
        // Calculate the modifier using d20 rules.
        //ability.mod = Math.floor((ability.value - 10) / 2);
        ability.defense = 10 + Number(abilityRoll.result);
      }
      
      await act.update({'data.abilities': abilis});
      
      // Hit Points
      console.log("Character Generating: Rolling Hit Points")
      let HPRoll = new Roll("1d8");
      await HPRoll.roll();
      
      
      let newHealth = {
        value: HPRoll.result,
        min: 0,
        max: HPRoll.result
      }
      
      act.update({'data.health': newHealth});
      

      // If the corresponding game setting is on, clear the inventory
      if (game.settings.get("mouses-knave", "clearInvOnGen")) {

        console.log("Character Generating: Clearing Inventory");

        let ids = [];

        html.find('.item-delete').each((index, btn) => {
          let par = $(btn).parents(".item");
          let id = par.data("itemId");
          ids.push(id);
        });

        ids.forEach(id => {
          act.items.get(id).delete()
        })


        // Set all coins to 0 too.
        await act.update({
          'data.coins.copper': 0,
          'data.coins.silver': 0,
          'data.coins.gold': 0,
          'data.coins.platinum': 0,
        })

      }
      
      // All characters start with 2 days of travel rations
      const toolGearPack = game.packs.get("mouses-knave.tools-and-gear");
      const toolGearIndex = await toolGearPack.getIndex();
      
      const rationId = toolGearIndex.find(e => e.name == "Travel Rations (1 day)")._id;
      //console.log(rationId);

      const l = await toolGearPack.getDocument(rationId)

      await Item.create(l.data, {parent: act})


      /* ROLL ON GEAR TABLES */
      // Find the pack with all the rollable tables in it
      const tablePack = game.packs.get("mouses-knave.character-generation-tables");
      const packIndex = await tablePack.getIndex();
      

      /* Starting Armor */
      console.log("Character Generating: Generating Armor");
      
      // Reset Armor
      await act.update({
        'data.attributes.armor.defense': 10
      });

      // Find the armor table
      const armorTableId = packIndex.find(e => e.name == "Armor")._id;
      const armorTable = await tablePack.getDocument(armorTableId);


      /* Roll for a random armor! */
      const armorDraw = await armorTable.roll()
      const armorResults = armorDraw.results[0]

      const armorPack = await game.packs.get("mouses-knave.armor")

      if (armorResults.data.resultId) {
        let di = await armorPack.getDocument(armorResults.data.resultId)
        await Item.create(di.data, {parent: act})

        let newArmorDefense
        if (di.data.name == "Gambeson") {
          newArmorDefense = 10 + 2 // 12
        } else if (di.data.name == "Brigandine") {
          newArmorDefense = 10 + 3 // 13
        } else if (di.data.name == "Chain") {
          newArmorDefense = 10 + 4
        }

        await act.update({
          'data.attributes.armor.defense': newArmorDefense
        })

      }


      /* Starting Helmet / Shield */
      console.log("Character Generating: Generating Helmet / Shield");
      
      const hsTableId = await packIndex.find(e => e.name == "Helmets and Shields")._id;
      const hsTable = await tablePack.getDocument(hsTableId);
      
      const hsDraw = await hsTable.roll()
      const hsResult = hsDraw.results[0]

      if (hsResult.data.resultId) {
        const armorPack = game.packs.get("mouses-knave.armor")
        
        let hs = await armorPack.getDocument(hsResult.data.resultId)
        await Item.create(hs.data, {parent: act})

        let newArmor = act.data.data.attributes.armor.defense + 1

        // Increment Defense By One
        act.update({
          'data.attributes.armor.defense':  newArmor
        })
          
      } else if (hsDraw.text == "Helmet and Shield") {
          // Find the helmet and shield's id
          
          const armorPack = await game.packs.get("mouses-knave.armor");
          const hsPackIndex = await armorPack.getIndex();
          
          const helmId = hsPackIndex.find(e => e.name == "Helmet")._id;
          const shieldId = hsPackIndex.find(e => e.name == "Shield")._id;
    
          const helm = await armorPack.getDocument(helmId)
          const shield = await armorPack.getDocument(shieldId)

          await Item.create(helm.data, {parent: act})
          await Item.create(shield.data, {parent: act})

          //await act.importItemFromCollection("mouses-knave.armor", helmId);
          //await act.importItemFromCollection("mouses-knave.armor", shieldId);
          
          // Increment Defense By Two
          let newArmor = act.data.data.attributes.armor.defense + 2
          
          await act.update({
            'data.attributes.armor.defense':  newArmor
          })
        
      }
       

      /* Starting Gear */
      console.log("Character Generating: Dungeoneering Gear");
      
      const dunGearId = packIndex.find(e => e.name == "Dungeoneering Gear")._id;
      const dunGearTable = await tablePack.getDocument(dunGearId);
      
      const dunGearPack = game.packs.get("mouses-knave.dungeoneering-gear")

      const dunGearDraw = await dunGearTable.roll()
      const dunGearResult = dunGearDraw.results[0]

      const dunGearItem = await dunGearPack.getDocument(dunGearResult.data.resultId)
      await Item.create(dunGearItem.data, {parent: act})

      
      const dunGearDraw2 = await dunGearTable.roll()
      const dunGearResult2 = dunGearDraw2.results[0]

      const dunGearItem2 = await dunGearPack.getDocument(dunGearResult2.data.resultId)
      await Item.create(dunGearItem2.data, { parent: act })
      
      /* General Gear */
      console.log("Character Generating: General Gear 1");
      
      const gg1Id = packIndex.find(e => e.name == "General Gear 1")._id;
      const gg1Table = await tablePack.getDocument(gg1Id);
      const gg1Pack = game.packs.get("mouses-knave.general-gear-1")
      
      const gg1Draw = await gg1Table.roll()
      const gg1Result = gg1Draw.results[0];
      const gg1Item = await gg1Pack.getDocument(gg1Result.data.resultId)
      await Item.create(gg1Item.data, {parent: act})


      console.log("Character Generating: General Gear 2");
      
      const gg2Id = packIndex.find(e => e.name == "General Gear 2")._id;
      const gg2Table = await tablePack.getDocument(gg2Id);
      const gg2Pack = game.packs.get("mouses-knave.general-gear-2")

      const gg2Draw = await gg2Table.roll()
      const gg2Result = gg2Draw.results[0];
      const gg2Item = await gg2Pack.getDocument(gg2Result.data.resultId)
      await Item.create(gg2Item.data, { parent: act })
     
      
    });
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  async _onItemCreate(event) {
    const context = this.getData();

    // Check for encumberance!!
    let makeItemSmall = false;
    if (context.data.data.attributes.slotsremaining <= 0) {
      ui.notifications.warn("Not enough item slots!");
      return false;
    } else if (context.data.data.attributes.slotsremaining < 1) {
      makeItemSmall = true;
    }

    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // If the inventory has less than one but greater than 0 slots left, make the item a quarter size
    if (makeItemSmall) itemData.data.slotstaken = 0.25;

    // Finally, create the item!
    return await Item.create(itemData, {parent: context.actor})
  }

  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  async _onRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    //console.log(dataset)

    let context = this.getData()
    console.log(context)

    //console.log(this.actor.data.data)

    if (dataset.roll) {
      let roll = new Roll(dataset.roll, context.data.data);
      let label = dataset.label ? `Rolling ${dataset.label}` : '';

      //console.log(roll)

      let r = await roll.roll()
      
      r.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: context.actor }),
        flavor: label
      });
    }
  }

  async _onDrop(event) {
    
    // Try to extract the data
    let data;
    try {
      data = JSON.parse(event.dataTransfer.getData('text/plain'));
      if (data.type !== "Item") return;
    } catch (err) {
      return false;
    }
    
    // Test the item's encumberance against the current amount of slots left open
    let itemSlotsTaken;

    // We have to do things a bit different if the item is from a compendium.
    if (data.pack) {
      
      let itemFromCompendium = await game.packs.get(data.pack).getEntry(data.id);
      itemSlotsTaken = itemFromCompendium.data.slotstaken;
      
    
    // Things also change if we provide data explicitly, such as dragging from another player.
    } else if (data.data) {
      itemSlotsTaken = Number(data.data.data.slotstaken);

    } else {
      itemSlotsTaken = game.items.get(data.id).data.data.slotstaken
    }

    let context = this.getData();
    if (context.data.data.attributes.slotsremaining < itemSlotsTaken) {
      ui.notifications.warn("Not enough item slots!");
      return false;
    }

    // Case 1 - Import from a Compendium pack
    const actor = context.actor;
    if (data.pack) {
      return actor.importItemFromCollection(data.pack, data.id);
    }
    // Case 2 - Data explicitly provided
    else if (data.data) {
      let sameActor = data.actorId === actor._id;
      if (sameActor && actor.isToken) sameActor = data.tokenId === actor.token.id;
      if (sameActor) return this._onSortItem(event, data.data); // Sort existing items
      else return actor.createEmbeddedEntity("OwnedItem", duplicate(data.data)); // Create a new Item
    }
    // Case 3 - Import from World entity
    else {
      let item = game.items.get(data.id);
      console.log(item)
      if (!item) return;
      //return actor.createEmbeddedEntity("OwnedItem", duplicate(item.data));
      return await Item.create(item.data, {parent: actor})
    }
  }

}
